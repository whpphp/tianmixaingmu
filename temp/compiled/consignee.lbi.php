<?php echo $this->smarty_insert_scripts(array('files'=>'utils.js,transport_jquery.js')); ?>
<div class="checkout-box">

      <h2 class="aui_title" style="cursor: move;"><?php echo $this->_var['lang']['consignee_info']; ?></h2>
      
      <ul class="box-main clearfix" id="addr-form">
            <?php if ($this->_var['real_goods_count'] > 0): ?>
             
            <li class="section-options clearfix"> 
              <label class="section-header"><?php echo $this->_var['lang']['country_province']; ?>:</label>
              <div class="section-body section-address">
              	  <div class="dropdown">
                  	  <label class="iconfont"></label>
                      <select name="country" id="selCountries_<?php echo $this->_var['sn']; ?>" onchange="region.changed(this, 1, 'selProvinces_<?php echo $this->_var['sn']; ?>')" class="input-select">
                        <option value="0"><?php echo $this->_var['lang']['please_select']; ?><?php echo $this->_var['name_of_region']['0']; ?></option>
                        <?php $_from = $this->_var['country_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'country');if (count($_from)):
    foreach ($_from AS $this->_var['country']):
?>
                        <option value="<?php echo $this->_var['country']['region_id']; ?>" <?php if ($this->_var['consignee']['country'] == $this->_var['country']['region_id']): ?>selected<?php endif; ?>><?php echo $this->_var['country']['region_name']; ?></option>
                        <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                      </select>
                  </div>
              	  <div class="dropdown">
                  	  <label class="iconfont"></label>
                      <select name="province" id="selProvinces_<?php echo $this->_var['sn']; ?>" onchange="region.changed(this, 2, 'selCities_<?php echo $this->_var['sn']; ?>')" class="input-select">
                        <option value="0"><?php echo $this->_var['lang']['please_select']; ?><?php echo $this->_var['name_of_region']['1']; ?></option>
                        <?php $_from = $this->_var['province_list'][$this->_var['sn']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'province');if (count($_from)):
    foreach ($_from AS $this->_var['province']):
?>
                        <option value="<?php echo $this->_var['province']['region_id']; ?>" <?php if ($this->_var['consignee']['province'] == $this->_var['province']['region_id']): ?>selected<?php endif; ?>><?php echo $this->_var['province']['region_name']; ?></option>
                        <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                      </select>
                  </div>
              	  <div class="dropdown">
                  	  <label class="iconfont"></label>
                      <select name="city" id="selCities_<?php echo $this->_var['sn']; ?>" onchange="region.changed(this, 3, 'selDistricts_<?php echo $this->_var['sn']; ?>')" class="input-select">
                        <option value="0"><?php echo $this->_var['lang']['please_select']; ?><?php echo $this->_var['name_of_region']['2']; ?></option>
                        <?php $_from = $this->_var['city_list'][$this->_var['sn']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'city');if (count($_from)):
    foreach ($_from AS $this->_var['city']):
?>
                        <option value="<?php echo $this->_var['city']['region_id']; ?>" <?php if ($this->_var['consignee']['city'] == $this->_var['city']['region_id']): ?>selected<?php endif; ?>><?php echo $this->_var['city']['region_name']; ?></option>
                        <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                      </select>
                  </div>
              	  <div class="dropdown">
                  	  <label class="iconfont"></label>
                      <select name="district" id="selDistricts_<?php echo $this->_var['sn']; ?>" <?php if (! $this->_var['district_list'][$this->_var['sn']]): ?>style="display:none"<?php endif; ?> class="input-select">
                        <option value="0"><?php echo $this->_var['lang']['please_select']; ?><?php echo $this->_var['name_of_region']['3']; ?></option>
                        <?php $_from = $this->_var['district_list'][$this->_var['sn']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'district');if (count($_from)):
    foreach ($_from AS $this->_var['district']):
?>
                        <option value="<?php echo $this->_var['district']['region_id']; ?>" <?php if ($this->_var['consignee']['district'] == $this->_var['district']['region_id']): ?>selected<?php endif; ?>><?php echo $this->_var['district']['region_name']; ?></option>
                        <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                      </select>
                  </div>
              </div>
             </li>
            <?php endif; ?>
            <li class="section-options clearfix">
              <label class="section-header"><em>*</em><?php echo $this->_var['lang']['consignee_name']; ?></label>
              <div class="section-body">
                  <input name="consignee" type="text" class="input-text" id="consignee_<?php echo $this->_var['sn']; ?>" value="<?php echo htmlspecialchars($this->_var['consignee']['consignee']); ?>" />
              </div>
            </li>
            <li class="section-options clearfix">
              <label class="section-header"><em>*</em><?php echo $this->_var['lang']['email_address']; ?></label>
              <div class="section-body">
                  <input name="email" type="text" class="input-text" id="email_<?php echo $this->_var['sn']; ?>" value="<?php echo htmlspecialchars($this->_var['consignee']['email']); ?>" />
              </div>
             </li>
            
            <?php if ($this->_var['real_goods_count'] > 0): ?> 
            
            <li class="section-options clearfix">
              <label class="section-header"><em>*</em><?php echo $this->_var['lang']['detailed_address']; ?></label>
              <div class="section-body">
                  <input name="address" type="text" class="input-text"  id="address_<?php echo $this->_var['sn']; ?>" value="<?php echo htmlspecialchars($this->_var['consignee']['address']); ?>" />
              </div>
              </li>
            <li class="section-options clearfix">
              <label class="section-header"><?php echo $this->_var['lang']['postalcode']; ?></label>
              <div class="section-body">
                  <input name="zipcode" type="text" class="input-text" id="zipcode_<?php echo $this->_var['sn']; ?>" value="<?php echo htmlspecialchars($this->_var['consignee']['zipcode']); ?>" />
              </div>
              </li>
            <?php endif; ?>
            <li class="section-options clearfix">
              <label class="section-header"><em>*</em><?php echo $this->_var['lang']['phone']; ?></label>
              <div class="section-body">
                  <input name="tel" type="text" class="input-text" id="tel_<?php echo $this->_var['sn']; ?>" value="<?php echo htmlspecialchars($this->_var['consignee']['tel']); ?>" />
              </div>
              </li>
            <li class="section-options clearfix">
              <label class="section-header"><?php echo $this->_var['lang']['backup_phone']; ?></label>
              <div class="section-body">
                  <input name="mobile" type="text" class="input-text" id="mobile_<?php echo $this->_var['sn']; ?>" value="<?php echo htmlspecialchars($this->_var['consignee']['mobile']); ?>" />
              </div>
            </li>
            
            <?php if ($this->_var['real_goods_count'] > 0): ?> 
            
            
            <li class="section-options clearfix">
              <label class="section-header"><?php echo $this->_var['lang']['sign_building']; ?></label>
              <div class="section-body">
                  <input name="sign_building" type="text" class="input-text" id="sign_building_<?php echo $this->_var['sn']; ?>" value="<?php echo htmlspecialchars($this->_var['consignee']['sign_building']); ?>" />
              </div>
            </li>
            <li class="section-options clearfix">
              <label class="section-header"><?php echo $this->_var['lang']['deliver_goods_time']; ?></label>
              <div class="section-body">
                  <input name="best_time" type="text" class="input-text" id="best_time_<?php echo $this->_var['sn']; ?>" value="<?php echo htmlspecialchars($this->_var['consignee']['best_time']); ?>" />
              </div>
            </li>
            <?php endif; ?>
        </ul>
        
            
    <div class="form-confirm clearfix">
        <input type="submit" name="Submit" class="btn btn-primary" value="<?php echo $this->_var['lang']['shipping_address']; ?>" />
        <input type="hidden" name="step" value="consignee" />
        <input type="hidden" name="act" value="checkout" />
        <input name="address_id" type="hidden" value="<?php echo $this->_var['consignee']['address_id']; ?>" />
        
        <?php if ($_SESSION['user_id'] > 0 && $this->_var['consignee']['address_id'] > 0): ?> 
        
        <a href="javascript:;" onclick="if (confirm('<?php echo $this->_var['lang']['drop_consignee_confirm']; ?>')) location.href='flow.php?step=drop_consignee&amp;id=<?php echo $this->_var['consignee']['address_id']; ?>'"   hidefocus="true" class="btn btn-gray">删 除</a> <?php endif; ?> 
    
    </div>
</div>